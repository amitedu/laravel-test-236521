<?php

namespace App;

use App\Customer;
use App\Orderdetail;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function orderdetails()
    {
        return $this->hasMany(Orderdetail::class, 'orderNumber');
    }

    public function customers()
    {
        return $this->belongsTo(Customer::class);
    }
}
