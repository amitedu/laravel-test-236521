<?php

namespace App;

use App\Orderdetail;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function orderdetails()
    {
        return $this->hasOne(Orderdetail::class);
    }
}
