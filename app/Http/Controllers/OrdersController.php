<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Order;
use App\Orderdetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrdersController extends Controller
{

    /**
     * fetch order details
     * @param $id
     * @return array
     */
    public function fetchOrderData($id)
    {
        // your logic goes here.

        // Through Query Builder

        $order_details = DB::table('orders')
                            ->where('orderNumber', $id)
                            ->select('orderNumber', 'orderDate', 'status')
                            ->get()
                            ->toArray();

        $order_details2 = Order::where('orderNumber', $id)->select('orderNumber', 'orderDate', 'status')
        ->get()->toArray();

        $order_details3 = array_merge(...array_values($order_details2));

        // dd($order_details3);

        $orderProductDetails = DB::table('orderdetails')
                    ->join('products', 'orderdetails.productCode', '=', 'products.productCode')
                    ->select(
                        'products.productName as product',
                        'products.productLine as product_line',
                        'orderdetails.priceEach as unit_price',
                        'orderdetails.quantityOrdered as qty',
                        DB::raw('(orderdetails.priceEach * orderdetails.quantityOrdered) as line_total')
                    )
                    ->where('orderdetails.orderNumber', $id)
                    ->get();

        // $total_price = $order_details->map(function($value) {
        //     return $value->line_total;
        // });

        $sum = $orderProductDetails->sum(function ($value) {
            return $value->line_total;
        });


        $customer = DB::table('orders')
                    ->join('customers', 'orders.customerNumber', '=', 'customers.customerNumber')
                    ->select(
                        'customers.contactFirstName as first_name',
                        'customers.contactLastName as last_name',
                        'customers.phone',
                        'customers.country as country_code'
                    )
                    ->where('orders.orderNumber', $id)
                    ->get();

        // dd($customer);

        // dd($order_details);

        // $ordId = Orderdetail::with('orders')->where('orderNumber', $id)->get();

        // $productCode = $ordId->pluck('productCode')->toArray();

        // $ordId = $ordId->toArray();

        // $orderDetails = array_merge(...array_values($ordId));

        // $orders = Order::select('orderNumber', 'orderDate', 'status', 'customerNumber')->where('orderNumber', $id)->get()->toArray();

        // $customer = Customer::select('contactFirstName', 'contactLastName', 'phone', 'country')->where('customerNumber', $orders[0]['customerNumber'])->get()->toArray();

        // $customer = array_merge(...array_values($customer));

        // $ordId = Orderdetail::with(['products' => function ($query) use ($id) {
        //     $query->where('orderNumber', $id);
        // }])->get();

        // $ordId = Orderdetail::whereHas('products', function ($query, $id) {
        //     $query->where('orderNumber', $id);
        // })->with('products')->get();

        // $ordId = Orderdetail::with('products')->where('orderNumber', $id)->get();

        // dd($ordId);

        // dd($ordId);

        return [
            'order_id' => $order_details3['orderNumber'],
            'order_date' => $order_details3['orderDate'],
            'status' => $order_details3['status'],
            'order_details' => $orderProductDetails,
            'bill_amount' => $sum,
            'customer' => $customer,
            // 'customer' => json_encode($customer),
        ];
    }
}
